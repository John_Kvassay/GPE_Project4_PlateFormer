﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevel3 : MonoBehaviour {

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == GameManager.instance.Player.gameObject)
        {
            GameManager.instance.LoadLevel3();
        }

    }

      
}
