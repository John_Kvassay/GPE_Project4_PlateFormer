﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public GameObject Player;
	public float jumpforce; // Determines how high the player can jump
	public float movespeed; // Determines player movement speed

    // Makes the interdependencies public to the script
    SpriteRenderer playersprite;
    Rigidbody2D rb2d;
    

    void Awake()
    {
        // Interdependencies must be on either void Start, or void Awake
        // Used for flipping the character along the X Axis
        playersprite = GetComponent<SpriteRenderer>();
        rb2d = GetComponent<Rigidbody2D>();
    }


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
    // Controls for player movement
	void Update () {

		if (Input.GetKey (KeyCode.LeftArrow)) 
		{
			Player.transform.position += new Vector3 (-movespeed, 0, 0) * Time.deltaTime;
            playersprite.flipX = true; // Flips the player on the x Axis
		}

		if (Input.GetKey (KeyCode.RightArrow)) 
		{
			Player.transform.position += new Vector3 (movespeed, 0, 0) * Time.deltaTime;
            playersprite.flipX = false; // Flips the player on the x Axis
		}

		if (Input.GetKeyDown (KeyCode.UpArrow)) 
		{
			Player.transform.position += new Vector3 (0, jumpforce, 0) * Time.deltaTime;
		}


	}
}
